#!/bin/bash

WPS_SKIP_ASSOCHECK_OVERRIDE="${WPS_SKIP_ASSOCHECK_OVERRIDE:-0}"

if [[ "$@" =~ .*/assocheck.sh.* ]] && [ "$WPS_SKIP_ASSOCHECK_OVERRIDE" = 0 ]
	then
		echo_err "assocheck.sh from WPS Office has been overriden!"
		exit 0
	else
		exec /bin/bash "$@"
fi
