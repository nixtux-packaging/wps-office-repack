#!/bin/bash
set +e
set +f
set -u

WPS_SKIP_ASSOCHECK_OVERRIDE="${WPS_SKIP_ASSOCHECK_OVERRIDE:-0}"

_echo_err(){
	echo "$@" 1>&2
}

_cleanup_mime_junk(){
	# accelerate, to avoid running update-mime-database
	if [ ! -f "$HOME"/.local/share/mime/application/wps-office.docx.xml ]; then
		return 0
	fi

	rm -fv "$HOME"/.local/share/mime/application/wps-office*.xml

	if [ -f "$HOME"/.local/share/mime/packages/Override.xml ] && [ "$(md5sum "$HOME"/.local/share/mime/packages/Override.xml | awk '{print $1}')" = "3171b60ed589736b09b7c2250ca5c8ac" ]; then
		rm -fv "$HOME"/.local/share/mime/packages/Override.xml
	fi

	update-mime-database "$HOME"/.local/share/mime
}

# Open hyperlinks in a normal browser, not WPS's embeded browser
_hack_db(){
	if [ ! -d "$HOME/.local/share/Kingsoft/wps" ] || [ -f "$HOME/.local/share/Kingsoft/wps/.hacked_db.status" ]; then
		return 0
	fi
	# ~/.local/share/Kingsoft/wps/addons/data/win-i386/promebrowser/Preferences
	find "$HOME/.local/share/Kingsoft/wps" -name Preferences -print | while read -r line
	do
		# SQL generated with GUI sqlitebrowser
		sqlite3 "$line" 'UPDATE "main"."global_preference" SET "useDefaultBrowser"=1 WHERE "_rowid_"="1";' && \
		touch "$HOME/.local/share/Kingsoft/wps/.hacked_db.status"
	done
}

_cleanup_mime_junk
_hack_db
