#!/usr/bin/env bash

set -xeu
dir0="$PWD"

# %{buildroot} = ${DESTDIR}
DESTDIR=${DESTDIR:-${PWD}}
# http://wps-community.org/downloads?vl=2019
DEB=orig/wps-office_${VERSION}_${ARCH}.deb
[ -f ${DEB} ] || ( echo "File ${DEB} not found!" && exit 1 )
mkdir -p ${DESTDIR}
pushd ${DESTDIR}
	ar p ${dir0}/${DEB} data.tar.xz | xz -d -v > wps-office_${VERSION}_${ARCH}.tar
	tar -xvf wps-office_${VERSION}_${ARCH}.tar
	rm -fv wps-office_${VERSION}_${ARCH}.tar #${dir0}/${DEB} 
popd

rsync -av files_examples ${DESTDIR}/opt/kingsoft/wps-office/
rsync -av dictionaries/* ${DESTDIR}/opt/kingsoft/wps-office/office6/dicts/spellcheck/
#rm -fv ${DESTDIR}/usr/share/applications/*.desktop
#rsync -av desktop_files ${DESTDIR}/usr/share/applications/
rm -fv ${DESTDIR}/usr/share/applications/appurl.desktop
rm -fvr ${DESTDIR}/usr/share/mime/
rm -fvr ${DESTDIR}/usr/share/desktop-directories/
rm -fvr ${DESTDIR}/etc/xdg/menus/applications-merged/
rm -fvr ${DESTDIR}/etc/cron.d/
rm -fvr ${DESTDIR}/etc/logrotate.d/
rm -fvr ${DESTDIR}/etc/xdg/autostart/
rm -fv ${DESTDIR}/opt/wps-office/office6/cfgs/domain_qing.cfg
mkdir -p ${DESTDIR}/etc/fonts/conf.avail/
mkdir -p ${DESTDIR}/etc/fonts/conf.d/
rsync -av fontconfig/* ${DESTDIR}/etc/fonts/conf.avail/

# there is another desktop file for /usr/bin/wps
rm -fv ${DESTDIR}/usr/share/applications/wps-office-prometheus.desktop

pushd ${DESTDIR}/etc/fonts/conf.d/
for i in $(ls ../conf.avail/)
do
	ln -sf "../conf.avail/${i}" "$i"
done
popd

pushd ${DESTDIR}
	patch -p0 < ${dir0}/wps-office-configs.patch
	# Remove address of update server (XXX does not work)
	# Not included into patch because of problems with encoding
	sed -i -e '/Address/d' opt/kingsoft/wps-office/office6/cfgs/setup.cfg
	patch -p0 < ${dir0}/wps-office-avoid-mime-junk.patch
popd

rm -fr wps_i18n_tmp
cp -r wps_i18n wps_i18n_tmp
pushd wps_i18n_tmp
	patch -p1 < ../wps_i18n-install-dir.patch
	# lrelease etc.
	export PATH="/usr/lib/x86_64-linux-gnu/qt4/bin:${PATH}"
	export QT_SELECT=qt4
	make
	make install
popd
rm -fr wps_i18n_tmp

mkdir -p ${DESTDIR}/opt/kingsoft/wps-office/local_bin
install -m0755 wps-office-fake-bash.sh ${DESTDIR}/opt/kingsoft/wps-office/local_bin/bash
install -m0755 wps-office-cleanup.sh ${DESTDIR}/usr/bin/wps-office-cleanup
