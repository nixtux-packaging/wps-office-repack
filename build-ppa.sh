#!/bin/bash

#d rawtherapee-5.4

pkg_name="wps-office"
dir0="$(pwd)"
old_header=$(head -1 ./debian/changelog)

# wps-office eats too much space in the PPA
for i in focal #hirsute
do
	#sed  -i -re "1s/unstable/$i" ./debian/changelog
	old_version="$(cat ./debian/changelog | head -n 1 | awk -F "(" '{print $2}' | awk -F ")" '{print $1}')"
	new_version="${old_version}~${i}1"
	sed -i -re "s/${old_version}/${new_version}/g" ./debian/changelog
	sed -i -re "1s/unstable/$i/" ./debian/changelog
	dpkg-buildpackage -S -sa -d
	sed  -i -re "1s/.*/${old_header}/" ./debian/changelog
	cd ..
	
	for i in "ppa:mikhailnov/utils"
	do
		dput -f "$i" "$(ls -tr ${pkg_name}_*_source.changes | tail -n 1)" 
	done
	
	cd "${dir0}"
	sleep 1
done

cd ..
