ARCH=amd64
VERSION=11.1.0.10161.XA

all:
	echo "Run make install"

install:
	env DESTDIR=$(DESTDIR) VERSION=$(VERSION) ARCH=$(ARCH) bash wps-repack.sh

clean:
	rm -fvr etc/ opt/ usr/ wps_i18n_tmp/ debian/wps-office*
